var builder = WebApplication.CreateBuilder(args);

builder.Services.AddControllers();

var app = builder.Build();

app.UseRouting();

app.UseEndpoints(endpoints =>
{
    // Map the /version endpoint
    endpoints.MapGet("/version", async context =>
    {
        var assemblyVersion = typeof(Program).Assembly.GetName().Version.ToString();
        await context.Response.WriteAsync($"Version: {assemblyVersion}");
    });
});

app.Run();